# Changelog

## Unreleased

### Added

- CI

### Fixed

- Certains typages & phpdoc

## 2.0.2 - 2024-12-19

### Changed

- Dépendances spip-league/filesystem en 3.0

## 2.0.1 - 2024-11-30

### Changed

- Dépendances symfony 7.2 minimum.

## 2.0.0 - 2024-07-18

### Changed

- namespace PHP de `Spip` à `SpipLeague`
- PHP 8.2 mini
