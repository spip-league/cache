<?php

namespace SpipLeague\Component\Cache\Exceptions;

class InvalidArgumentException extends \InvalidArgumentException {}
