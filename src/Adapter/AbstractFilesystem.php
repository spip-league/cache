<?php

namespace SpipLeague\Component\Cache\Adapter;

use DateInterval;
use FilesystemIterator;
use Generator;
use Psr\Clock\ClockInterface;
use Psr\SimpleCache\CacheInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SpipLeague\Bridge\Filesystem\Filesystem as SpipFilesystem;
use SpipLeague\Component\Cache\Exceptions\InvalidArgumentException;
use SpipLeague\Component\Hasher\Hash32;
use SpipLeague\Component\Hasher\HashInterface;
use SpipLeague\Contracts\Filesystem\FilesystemInterface;
use Symfony\Component\Clock\Clock;
use Throwable;

use function dirname;
use function filemtime;
use function is_writable;
use function preg_match;
use function rtrim;
use function uniqid;
use function var_export;

/**
 * Save files in directory
 *
 * Inspired by kodus/file-cache
 */
abstract class AbstractFilesystem implements CacheInterface
{
    /**
     * @var string control characters for keys, reserved by PSR-16
     */
    private const PSR16_RESERVED = '/\{|\}|\(|\)|\/|\\\\|\@|\:/u';

    protected readonly string $directory;

    /**
     * @param string $name Subdirectory name
     * @param string $directory Base storage directory (default to sys_get_temp_dir())
     * @param int|DateInterval $default_ttl TTL. if int (seconds), needs to be > 0.
     */
    public function __construct(
        string $name,
        ?string $directory = null,
        protected readonly int|DateInterval $default_ttl = new DateInterval('P1M'),
        protected readonly FilesystemInterface $filesystem = new SpipFilesystem(),
        protected readonly HashInterface $hasher = new Hash32(),
        protected readonly ClockInterface $clock = new Clock(),
    ) {
        if ($name === '') {
            throw new InvalidArgumentException(sprintf('$name argument needs to be set'));
        }
        if (is_int($this->default_ttl) && $this->default_ttl <= 0) {
            throw new InvalidArgumentException(sprintf('$default_ttl argument must be positive'));
        }
        $directory ??= sys_get_temp_dir();
        $this->directory = rtrim($directory, '/') . DIRECTORY_SEPARATOR . $name;
    }

    public function get(string $key, mixed $default = null): mixed
    {
        $path = $this->getValidatedPath($key);
        $expires_at = @filemtime($path);
        if ($expires_at === false) {
            // file not found
            return $default;
        }
        if ($this->clock->now()->getTimestamp() >= $expires_at) {
            // file expired
            $this->filesystem->remove($path);

            return $default;
        }
        try {
            $data = include $path;
        } catch (Throwable) {
            // file syntax error
            $this->filesystem->remove($path);

            return $default;
        }

        // Returns only if not file collision.
        if (is_array($data) && array_key_exists($key, $data)) {
            return $data[$key];
        }

        return $default;
    }

    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        $path = $this->getValidatedPath($key);
        $directory = dirname($path);
        $this->mkdir($directory);
        $ttl = $this->getTTL($ttl);

        $tmp_path = $directory . DIRECTORY_SEPARATOR . uniqid('', true);
        if ($this->filesystem->write($tmp_path, $this->prepareValue([$key => $value])) === false) {
            return false;
        }
        try {
            $this->filesystem->touch($tmp_path, $ttl);
            $this->filesystem->rename($tmp_path, $path, true);
        } catch (Throwable) {
            $this->filesystem->remove($tmp_path);
            return false;
        }

        return true;
    }

    public function delete(string $key): bool
    {
        $path = $this->getValidatedPath($key);
        $this->filesystem->remove($path);
        return true;
    }

    public function clear(): bool
    {
        $this->filesystem->remove($this->directory);

        return true;
    }

    /**
     * Clean up expired cache-files.
     *
     * This method is outside the scope of the PSR-16 cache concept, and is specific to
     * this implementation, being a file-cache.
     *
     * In scenarios with dynamic keys (such as Session IDs) you should call this method
     * periodically - for example from a scheduled daily cron-job.
     */
    public function clearExpired(): void
    {
        $this->filesystem->remove($this->listPathsExpired());
    }

    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     */
    public function has(string $key): bool
    {
        return $this->get($key, $this) !== $this;
    }

    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $values = [];
        foreach ($keys as $key) {
            $values[$key] = $this->get($key) ?? $default;
        }

        return $values;
    }

    /**
     * @param iterable<string, mixed> $values
     */
    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        $ok = true;
        foreach ($values as $key => $value) {
            $ok = $this->set($key, $value, $ttl) && $ok;
        }

        return $ok;
    }

    public function deleteMultiple(iterable $keys): bool
    {
        $ok = true;
        foreach ($keys as $key) {
            $ok = $this->delete($key) && $ok;
        }

        return $ok;
    }

    protected function mkdir(string $directory): void
    {
        $this->filesystem->mkdir($this->directory);
        if (!is_writable($this->directory)) {
            throw new InvalidArgumentException("Cache directory is not writable: {$this->directory}");
        }
    }

    /**
     * Validate key and returns file cache key path
     */
    protected function getValidatedPath(string $key): string
    {
        $this->validateKey($key);
        return $this->getPath($key);
    }

    /**
     * Returns file cache key path
     */
    abstract protected function getPath(string $key): string;

    /**
     * Returns the ttl or default ttl
     */
    protected function getTTL(null|int|DateInterval $ttl): int
    {
        $ttl ??= $this->default_ttl;

        if (is_int($ttl)) {
            return $this->clock->now()
                ->getTimestamp() + $ttl;
        }

        return $this->clock->now()
            ->add($ttl)
            ->getTimestamp();
    }

    /**
     * Validate Key : can’t have PSR16 reserved char
     */
    protected function validateKey(string $key): void
    {
        if (preg_match(self::PSR16_RESERVED, $key, $match) === 1) {
            throw new InvalidArgumentException("invalid character in key: {$match[0]}");
        }
    }

    /**
     * Get the content to write into the file fache
     */
    protected function prepareValue(mixed $value): string
    {
        // return '<?php return ' . Symfony\Component\VarExporter\VarExporter::export($value) . ';';
        return '<?php return ' . var_export($value, true) . ';';
    }

    /**
     * @return Generator & iterable<string>
     */
    protected function listPaths(): Generator
    {
        $iterator = new RecursiveDirectoryIterator(
            $this->directory,
            FilesystemIterator::CURRENT_AS_PATHNAME | FilesystemIterator::SKIP_DOTS,
        );

        /** @var iterable<string> */
        $iterator = new RecursiveIteratorIterator($iterator);

        foreach ($iterator as $path) {
            yield $path;
        }
    }

    protected function listPathsExpired(): Generator
    {
        $now = $this->clock->now()
            ->getTimestamp();

        foreach ($this->listPaths() as $path) {
            if ($now > filemtime($path)) {
                yield $path;
            }
        }
    }
}
