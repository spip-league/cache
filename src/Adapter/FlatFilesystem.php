<?php

namespace SpipLeague\Component\Cache\Adapter;

use SpipLeague\Component\Cache\Exceptions\InvalidArgumentException;

use function preg_match;
use function sprintf;

/**
 * For (few) files in one directory
 *
 * This Adapter keeps the key as filename.
 */
class FlatFilesystem extends AbstractFilesystem
{
    private const AUTHORIZED_CHARS = '/[^-+_.A-Za-z0-9]/';

    protected function getPath(string $key): string
    {
        return $this->directory . DIRECTORY_SEPARATOR . $key . '.php';
    }

    /**
     * Validate Key : restrict to safe file name chars
     */
    protected function validateKey(string $key): void
    {
        if (preg_match(self::AUTHORIZED_CHARS, $key, $match)) {
            throw new InvalidArgumentException(sprintf(
                'Name contains "%s" but only characters in [-+_.A-Za-z0-9] are allowed.',
                $match[0],
            ));
        }
        parent::validateKey($key);
    }
}
