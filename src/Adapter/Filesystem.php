<?php

namespace SpipLeague\Component\Cache\Adapter;

/**
 * Save files in tree directory
 */
class Filesystem extends AbstractFilesystem
{
    protected function getPath(string $key): string
    {
        $hash = $this->hasher->hash($key);

        return $this->directory
            . DIRECTORY_SEPARATOR . $hash[0] . $hash[1]
            . DIRECTORY_SEPARATOR . $hash[2] . $hash[3]
            . DIRECTORY_SEPARATOR . substr($hash, 4)
            . '.php';
    }
}
