<?php

namespace SpipLeague\Component\Cache\Adapter;

/**
 * Limit cache files to 256² files
 */
class LimitedFilesystem extends Filesystem
{
    protected function getPath(string $key): string
    {
        $hash = $this->hasher->hash($key);

        return $this->directory
            . DIRECTORY_SEPARATOR . $hash[0] . $hash[1]
            . DIRECTORY_SEPARATOR . $hash[2] . $hash[3]
            . '.php';
    }
}
