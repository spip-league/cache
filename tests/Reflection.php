<?php

namespace SpipLeague\Test\Component\Cache;

use ReflectionClass;

class Reflection
{
    public static function getProperty(object $object, string $property): mixed
    {
        $class = new ReflectionClass($object);
        $property = $class->getProperty($property);
        $property->setAccessible(true);
        return $property->getValue($object);
    }

    /**
     * @param array<int|string,mixed> $args
     */
    public static function callMethod(object $object, string $method, array $args = []): mixed
    {
        $class = new ReflectionClass($object);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $args);
    }
}
