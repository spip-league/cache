<?php

namespace SpipLeague\Test\Component\Cache\Adapter;

use DateInterval;
use DateTimeImmutable;
use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\TestCase;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SpipLeague\Component\Cache\Adapter\AbstractFilesystem;
use SpipLeague\Component\Cache\Exceptions\InvalidArgumentException;
use SpipLeague\Component\Hasher\HashInterface;
use SpipLeague\Contracts\Filesystem\FilesystemInterface;
use SpipLeague\Test\Component\Cache\Reflection;
use Symfony\Component\Clock\MockClock;

class FilesystemTestCase extends TestCase
{
    #[DataProvider('providerFilesystem')]
    public function testIo(AbstractFilesystem $cache): void
    {
        $cache->clear();

        // inexistant
        $this->assertFalse($cache->has('not.exists'));
        $this->assertNull($cache->get('not.exists'));
        $this->assertTrue($cache->get('not.exists', true));
        $this->assertSame('string', $cache->get('not.exists', 'string'));

        // write array
        $data = [
            'null' => null,
            'bool' => true,
            'int' => 123,
            'float' => 1.23,
            'string int' => '321',
            'string float' => '3.21',
            'array' => [
                'y' => 'z',
            ],
            'object' => new DateTimeImmutable(),
        ];

        $this->assertTrue($cache->set('base', $data));
        $this->assertTrue($cache->has('base'));
        $this->assertEquals($data, $cache->get('base'));

        $directory = Reflection::getProperty($cache, 'directory');
        $this->assertIsString($directory);
        $this->assertDirectoryExists($directory);
        $this->assertCount(1, $this->listFiles($directory));

        // write string
        $this->assertTrue($cache->set('new', 'string'));
        $this->assertSame('string', $cache->get('new'));
        $this->assertCount(2, $this->listFiles($directory));

        // remove one
        $this->assertTrue($cache->delete('base'));
        $this->assertCount(1, $this->listFiles($directory));
        $this->assertSame('string', $cache->get('new'));

        // clear all
        $cache->clear();
        $this->assertDirectoryDoesNotExist($directory);

        // TTL int
        $this->assertTrue($cache->set('ttl.int.valid', 'string', 60));
        $this->assertSame('string', $cache->get('ttl.int.valid'));
        $this->assertCount(1, $this->listFiles($directory));

        // TTL interval
        $this->assertTrue($cache->set('ttl.interval.valid', 'string', new DateInterval('PT60S')));
        $this->assertSame('string', $cache->get('ttl.interval.valid'));
        $this->assertCount(2, $this->listFiles($directory));

        // TTL expired int
        $this->assertTrue($cache->set('ttl.int.expired', 'string', -60));
        $this->assertCount(3, $this->listFiles($directory));

        $this->assertNull($cache->get('ttl.int.expired'));
        $this->assertCount(2, $this->listFiles($directory));

        // TTL expired interval
        $this->assertTrue(
            $cache->set('ttl.interval.expired', 'string', DateInterval::createFromDateString('-60 seconds')),
        );
        $this->assertCount(3, $this->listFiles($directory));

        $this->assertNull($cache->get('ttl.interval.expired'));
        $this->assertCount(2, $this->listFiles($directory));

        // Clean Expired
        $this->assertTrue($cache->set('ttl.int.expired', 'string', -60));
        $this->assertCount(3, $this->listFiles($directory));

        $cache->clearExpired();
        $this->assertCount(2, $this->listFiles($directory));
    }

    #[DataProvider('providerFilesystem')]
    #[Depends('testIo')]
    public function testIoMultiple(AbstractFilesystem $cache): void
    {
        $cache->clear();
        $this->assertSame([
            'a' => null,
            'b' => null,
            'c' => null,
        ], $cache->getMultiple(['a', 'b', 'c']));
        $this->assertTrue($cache->setMultiple(['a' => 1, 'b' => 2, 'c' => 3]));
        $this->assertSame(['a' => 1, 'c' => 3], $cache->getMultiple(['a', 'c']));
        $this->assertTrue($cache->deleteMultiple(['a', 'b']));
        $this->assertSame(['a' => null, 'b' => null, 'c' => 3], $cache->getMultiple(['a', 'b', 'c']));
        $cache->clear();
    }

    #[DataProvider('providerFilesystemClassName')]
    #[Depends('testIoMultiple')]
    public function testDateExpiration(string $filesystemClassName): void
    {
        $clock = new MockClock();
        $cache = new $filesystemClassName('test-filesystem', default_ttl: 3600, clock: $clock);
        if (!$cache instanceof AbstractFilesystem) {
            $this->fail('oups.');
        }

        $cache->clear();

        $directory = Reflection::getProperty($cache, 'directory');
        $this->assertIsString($directory);

        $cache->set('one', 'string');
        $this->assertSame('string', $cache->get('one'));
        $this->assertCount(1, $this->listFiles($directory));

        $clock->sleep(4000);
        $this->assertSame(null, $cache->get('one'));
        $this->assertCount(0, $this->listFiles($directory));
    }

    #[DataProvider('providerFilesystem')]
    #[Depends('testIoMultiple')]
    public function testErrorPsr16Key(AbstractFilesystem $cache): void
    {
        $this->expectException(InvalidArgumentException::class);
        $cache->get('a{}');
    }

    #[DataProvider('providerFilesystem')]
    #[Depends('testIoMultiple')]
    public function testErrorInvalidExport(AbstractFilesystem $cache): void
    {
        $this->assertTrue($cache->set('class', [new class {}]));
        $this->assertNull($cache->get('class'));
    }

    public function doTestCacheCollision(string $filesystemClassName): void
    {
        $hasher = new class implements HashInterface {
            public function hash(mixed $data): string
            {
                return str_repeat('a', 8);
            }
        };
        $cache = new $filesystemClassName('test-filesystem', hasher: $hasher);
        if (!$cache instanceof AbstractFilesystem) {
            $this->fail('oups.');
        }

        $directory = Reflection::getProperty($cache, 'directory');
        $this->assertIsString($directory);

        $this->assertTrue($cache->set('collision.one', 'string.one'));
        $this->assertCount(1, $this->listFiles($directory));

        // should have rewrite first file
        $this->assertTrue($cache->set('collision.two', 'string.two'));
        $this->assertCount(1, $this->listFiles($directory));

        $this->assertSame('string.two', $cache->get('collision.two'));
        $this->assertSame(null, $cache->get('collision.one'));
        $this->assertSame('one', $cache->get('collision.one', 'one'));
        $cache->clear();
    }

    #[DataProvider('providerFilesystemClassName')]
    #[Depends('testIoMultiple')]
    public function testFilesystemEmptyNameException(string $filesystemClassName): void
    {
        $this->expectException(InvalidArgumentException::class);
        new $filesystemClassName('');
    }

    #[DataProvider('providerFilesystemClassName')]
    #[Depends('testIoMultiple')]
    public function testFilesystemNegativeTTLException(string $filesystemClassName): void
    {
        $this->expectException(InvalidArgumentException::class);
        new $filesystemClassName('test-filesystem', default_ttl: -100);
    }

    #[DataProvider('providerFilesystemClassName')]
    #[Depends('testIoMultiple')]
    public function testDirectoryNotWritable(string $filesystemClassName): void
    {
        $filesystem = $this->createConfiguredMock(FilesystemInterface::class, []);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('#^Cache directory is not writable#');
        $cache = new $filesystemClassName('test-filesystem-error', filesystem: $filesystem);
        if (!$cache instanceof AbstractFilesystem) {
            $this->fail('oups.');
        }

        $cache->set('a', 'dir');
    }

    #[DataProvider('providerFilesystemClassName')]
    #[Depends('testIoMultiple')]
    public function testWriteError(string $filesystemClassName): void
    {
        // create the directory
        $cache = new $filesystemClassName('test-filesystem-write');
        if (!$cache instanceof AbstractFilesystem) {
            $this->fail('oups.');
        }

        $cache->set('a', 'string');

        $filesystem = $this->createConfiguredMock(FilesystemInterface::class, [
            'write' => false,
        ]);
        $cache_not_writable = new $filesystemClassName('test-filesystem-write', filesystem: $filesystem);
        if (!$cache_not_writable instanceof AbstractFilesystem) {
            $this->fail('oups.');
        }

        $this->assertFalse($cache_not_writable->set('b', 'string'));

        $filesystem = $this->createConfiguredMock(FilesystemInterface::class, [
            'write' => true,
        ]);
        $filesystem->method('touch')
            ->willThrowException(new Exception());
        $cache_not_rename = new $filesystemClassName('test-filesystem-write', filesystem: $filesystem);
        if (!$cache_not_rename instanceof AbstractFilesystem) {
            $this->fail('oups.');
        }

        $this->assertFalse($cache_not_rename->set('c', 'string'));

        $cache->clear();
    }

    /**
     * @return RecursiveIteratorIterator<RecursiveDirectoryIterator>
     */
    protected function listFiles(string $directory): RecursiveIteratorIterator
    {
        return new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS),
        );
    }
}
