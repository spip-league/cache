<?php

namespace SpipLeague\Test\Component\Cache\Adapter;

use PHPUnit\Framework\Attributes\CoversClass;
use SpipLeague\Component\Cache\Adapter\AbstractFilesystem;
use SpipLeague\Component\Cache\Adapter\FlatFilesystem;

#[CoversClass(FlatFilesystem::class)]
#[CoversClass(AbstractFilesystem::class)]
class FlatFilesystemTest extends FilesystemTestCase
{
    public static function tearDownAfterClass(): void
    {
        (new FlatFilesystem('test-filesystem'))->clear();
        (new FlatFilesystem('test-filesystem-write'))->clear();
        (new FlatFilesystem('test-filesystem-error'))->clear();
    }

    public static function providerFilesystem(): \Generator
    {
        yield 'FlatFilesystem' => [new FlatFilesystem('test-filesystem')];
    }

    public static function providerFilesystemClassName(): \Generator
    {
        yield 'FlatFilesystemClass' => [FlatFilesystem::class];
    }
}
