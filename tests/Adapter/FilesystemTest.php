<?php

namespace SpipLeague\Test\Component\Cache\Adapter;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Depends;
use SpipLeague\Component\Cache\Adapter\AbstractFilesystem;
use SpipLeague\Component\Cache\Adapter\Filesystem;

#[CoversClass(Filesystem::class)]
#[CoversClass(AbstractFilesystem::class)]
class FilesystemTest extends FilesystemTestCase
{
    public static function tearDownAfterClass(): void
    {
        (new Filesystem('test-filesystem'))->clear();
        (new Filesystem('test-filesystem-write'))->clear();
        (new Filesystem('test-filesystem-error'))->clear();
    }

    public static function providerFilesystem(): \Generator
    {
        yield 'Filesystem' => [new Filesystem('test-filesystem')];
    }

    public static function providerFilesystemClassName(): \Generator
    {
        yield 'FilesystemClass' => [Filesystem::class];
    }

    #[DataProvider('providerFilesystemClassName')]
    #[Depends('testIoMultiple')]
    public function testCacheCollision(string $filesystemClassName): void
    {
        $this->doTestCacheCollision($filesystemClassName);
    }
}
