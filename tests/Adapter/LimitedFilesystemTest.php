<?php

namespace SpipLeague\Test\Component\Cache\Adapter;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Depends;
use SpipLeague\Component\Cache\Adapter\AbstractFilesystem;
use SpipLeague\Component\Cache\Adapter\LimitedFilesystem;

#[CoversClass(LimitedFilesystem::class)]
#[CoversClass(AbstractFilesystem::class)]
class LimitedFilesystemTest extends FilesystemTestCase
{
    public static function tearDownAfterClass(): void
    {
        (new LimitedFilesystem('test-filesystem'))->clear();
        (new LimitedFilesystem('test-filesystem-write'))->clear();
        (new LimitedFilesystem('test-filesystem-error'))->clear();
    }

    public static function providerFilesystem(): \Generator
    {
        yield 'LimitFilesystem' => [new LimitedFilesystem('test-filesystem')];
    }

    public static function providerFilesystemClassName(): \Generator
    {
        yield 'LimitFilesystemClass' => [LimitedFilesystem::class];
    }

    #[DataProvider('providerFilesystemClassName')]
    #[Depends('testIoMultiple')]
    public function testCacheCollision(string $filesystemClassName): void
    {
        $this->doTestCacheCollision($filesystemClassName);
    }
}
