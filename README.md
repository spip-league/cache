# Cache component

[PSR-16](https://www.php-fig.org/psr/psr-16/) Simple Cache implementation

This library implements for now 3 different strategies of filesystem cache. 

For other adapters (redis for example) or more advanced options, 
please use a more complete library like [symfony/cache](https://github.com/symfony/cache).

## Installation

```bash
composer require spip-league/cache
```

## Usage

### Basic usage

- Default directory is in OS temp directory
- Default TTL is 1 month

```php
use SpipLeague\Component\Cache\Adapter\Filesystem;

$cache = new Filesystem('name');
$cache->set('key', $data);

$data = $cache->get('key');
```

Specify storage directory, flat storage, default ttl

```php
use SpipLeague\Component\Cache\Adapter\FlatFilesystem;

$cache = new FlatFilesystem('name', '/path/to/my/cache/directory', 24 * 60 * 60);
$cache->has('key');
```

## Adapters

The filesystem adapters writes cache in valid PHP files,
that can also be cached by opcache. 

See [AbstractFilesystem constructor](src/Adapter/AbstractFilesystem.php#L46) for all arguments & types.

Default hashes (for path storage) are calculated with a default 32 bits hash, but you can pass a 128 bits hash (like `md5`), 
but hashing will be slower. See [spip-league/hasher](https://git.spip.net/spip-league/hasher)

Example:

```php
use SpipLeague\Component\Hasher\Hash128;
use SpipLeague\Component\Cache\Adapter\Filesystem;

$cache = new Filesystem('name', hasher: new Hash128());
```

### `Filesystem`

Files are saved in a tree directory. 

Example path: `{directory}/{name}/ab/cd/efgh.php`.

### `FlatFilesystem` 

Files are saved in the root of the directory and keeps the name of the key unchanged. Key names are validated with stricter chars than PSR-16 keys.

Example path: `{directory}/{name}/{key}.php`.


### `LimitedFilesystem` 

Files are saved in the root of a directory, but can’t be more than 256² files by construction.

Collisions could rewrite old file cache.

Example path: `{directory}/{name}/ab/cd.php`.

## Advanced usage

```php
use SpipLeague\Component\Cache\Adapter\LimitedFilesystem;

$directory = '/path/to/directory';

$cache = new LimitedFilesystem(
    'name', 
    $directory, 
    default_ttl: new DateInterval('P1W'), // 1 week
); 
$cache->set('key', $data);
$cache->set('key.short', $data, 3600); // 1 hour TTL for this key
$cache->set('key.other', $data, new DateInterval('PT6H')); // 6 hour ttl

$cache->has('key'); // bool

$cache->delete('key'); // bool

$cache->clear(); // remove all cache files
```

### Clean Expired

You should call periodically in cron job the (not in PSR-16) method `clearExpired`
that removes all expired files.

```php
// outside PSR-16 (removes expired files cache)
$cache->clearExpired(); 
```

## Tests

See Composer scripts in [composer.json](composer.json#L52)

```bash
composer test
composer test-coverage
```
